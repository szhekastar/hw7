
const arr =  ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const newArr = document.createElement('div');

function getArr (arr, parent = document.body) {
    const ul = document.createElement('ul');
    arr.map(item => {
        if(typeof item === 'object') {
            getArr (item,ul)
        }else{
            ul.insertAdjacentHTML(`beforeend`,`<li>${item}</li>`);
        }
    })
    parent.appendChild(ul)
}
getArr (arr,newArr)
document.body.appendChild(newArr)